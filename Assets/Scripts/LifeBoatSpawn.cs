﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBoatSpawn : MonoBehaviour {
	public GameObject enemyPrefab;
	public GameObject deadPrefab;
	public Transform spawnPoint;

	// Use this for initialization
	void Start () {
		spawnPoint = GetComponent<Transform> ();
	}
	IEnumerator SpawnAfterTime () {
		yield return new WaitForSeconds (1);
		Instantiate (enemyPrefab, spawnPoint.position, spawnPoint.rotation);
		Instantiate (deadPrefab, spawnPoint.position, spawnPoint.rotation);
		Destroy (this.gameObject);
	}
	void Update () {
		StartCoroutine (SpawnAfterTime ());
	}
}
