﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootLeft : MonoBehaviour {
	public GameObject cannonballPrefab;
	public Transform shootPoint;
	public float shootDelay;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		shootDelay = (Random.Range (.25f, .5f));
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			StartCoroutine (ShootAfterTime());
		}
	}
	IEnumerator ShootAfterTime () {
		yield return new WaitForSeconds (shootDelay);
		Instantiate (cannonballPrefab, shootPoint.position, shootPoint.rotation);
	}
}
