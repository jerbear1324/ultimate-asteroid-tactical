﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifeboats : MonoBehaviour {
	//public Transform player;
	private Transform myEnemy;
	public Transform targetTf;
	public float speed;
	private Transform tf;
	Vector3 movementVector;

	void Update () {
		movementVector = targetTf.position - tf.position;
		movementVector.Normalize ();
		movementVector = movementVector * speed;
		tf.position = tf.position + movementVector;
		tf.up = -movementVector;
		}
	void Start () {
		targetTf = GameObject.FindGameObjectWithTag ("Player").transform;
		tf = GetComponent<Transform> ();
		myEnemy = GetComponent<Transform> ();
	}
	void OnTriggerEnter2D ( Collider2D otherCollider) {
		Destroy (this.gameObject);
	}
	void Awake() {

	}
}
