﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball : MonoBehaviour {
	public Transform tf;
	public float speed;
	//private int divider = 1 / 10;
	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	// Update is called once per frame
	void Update () {
		StartCoroutine (killBullet ());
		Move ();
	}
	public void Move () {
		tf.position += tf.right * speed;
	}
	public void OnTriggerEnter2D ( Collider2D otherCollider) {
		Destroy (this.gameObject);
	}
	IEnumerator killBullet ()
	{
		yield return new WaitForSeconds (2);
		Destroy (gameObject);
	}
}
