﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heatseeker : MonoBehaviour {
	//public Transform player;
	private Transform myEnemy;
	public Transform targetTf;
	public float speed;
	private Transform tf;
	Vector3 movementVector;
	public bool isAlwaysSeeking;
	public bool isDirectional;
	public GameObject myBoat1;
	public GameObject myBoat2;
	private KillSelf deathRun;

	// Update is called once per frame
	void Start () {
		myEnemy = GetComponent<Transform> ();
		deathRun = GetComponent<KillSelf>();
	}
	void Update () {
		targetTf = GameObject.FindGameObjectWithTag ("Player").transform;
		//player = GameObject.FindGameObjectWithTag ("Player").transform;
		tf = GetComponent<Transform> ();
		movementVector = targetTf.position - tf.position;
		if (isAlwaysSeeking) {
			movementVector = targetTf.position - tf.position;
		}
		movementVector.Normalize ();
		movementVector = movementVector * speed;
		tf.position = tf.position + movementVector;
		//Movement

		if (isDirectional) {
			tf.up = -movementVector;
		}
	}
	void OnTriggerEnter2D ( Collider2D otherCollider) {
		myBoat1.SetActive(true);
		myBoat2.SetActive (true);
		Destroy (this.gameObject);
	}
}
