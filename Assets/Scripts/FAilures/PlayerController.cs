﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float speed = 1.0f;
	//allows for a designer to change the speed
	//need to find out how to set it as a fixed number
	//currently speed needs to be a decimal, not optimal
	public float rotationSpeed = 1.0f;
	//allows for a designer to set the turning speed of the car

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var x = Input.GetAxis ("Horizontal") * rotationSpeed;
		//controls for turning
		var z = Input.GetAxis ("Vertical") * speed;
		//Controls for gas
		//need to find a way for brake to work
		//need to find a way for gas to diminish over time

		transform.Rotate(0, x, 0);
		//this is for the cars turning
		transform.Translate(0, 0, z);
		//this is to check to see how fast the car should be going
		
	}
}
