﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour {
	private Transform enemy;
	void Awake ()
	{
		enemy = GameObject.FindGameObjectWithTag ("Enemy").transform;
	}
	public void OnTriggerEnter2D ( Collider2D otherCollider ) {
		if (enemy.tag == "Enemy") {
			Destroy (enemy.gameObject);
			//Kind of pointless at the moment
		}
	}
}