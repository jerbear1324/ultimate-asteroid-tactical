﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public Transform player;
	//public float movementSpeed = 0.0f;
	//public int maxDist = 0;
	//public int minDist = 0;
	//Was trying to make a enter trigger box chase player.
	private Transform myEnemy;

	// Use this for initialization
	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		
	}
	void Start () {
		myEnemy = GetComponent<Transform> ();
	}
	void Update () {
		//if (player != null) {
			//transform.LookAt (player);
			//Does not rotate on 2D plane
			//myEnemy.position -= myEnemy.up * movementSpeed;
			//Debug.Log ("Move");
	}
	void OnTriggerEnter2D ( Collider2D otherCollider) {
		Destroy (this.gameObject);
		//Cheap way to kill them without destroying colliders too
	}
}