﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColdSeeker : MonoBehaviour {
	//public Transform player;
	private Transform myEnemy;
	public Transform targetTf;
	public float speed;
	private Transform tf;
	Vector3 movementVector;
	public bool isAlwaysSeeking;
	public bool isDirectional;

	// Update is called once per frame
	void Start () {
		myEnemy = GetComponent<Transform> ();
	}
	void Update () {
		targetTf = GameObject.FindGameObjectWithTag ("Player").transform;
		//player = GameObject.FindGameObjectWithTag ("Player").transform;
		tf = GetComponent<Transform> ();
		movementVector = targetTf.position - tf.position;
		if (isAlwaysSeeking) {
			movementVector = targetTf.position - tf.position;
		}
		movementVector.Normalize ();
		movementVector = movementVector * speed;
		tf.position = tf.position + movementVector;
		//Movement

		if (isDirectional) {
			tf.up = -movementVector;
		}
	}
}
