﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public GameObject enemyPrefab;
	public Transform spawnPoint;
	private float spawnDelay;
	public float spawnTime = 3f;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
		spawnPoint = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update() {
		spawnDelay = (Random.Range (8, 15));
	}

	void Spawn () {
		StartCoroutine (SpawnAfterTime ());
	}

	IEnumerator SpawnAfterTime () {
		yield return new WaitForSeconds (spawnDelay);
		Instantiate (enemyPrefab, spawnPoint.position, spawnPoint.rotation);
	}
}
