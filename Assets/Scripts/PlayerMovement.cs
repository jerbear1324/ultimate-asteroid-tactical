﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float speed = 0.0f;
	//public for designers
	public float rotationSpeed = 0.0f;
	//public for designers
	public float reverseSpeed = 0.0f;
	//public for designers
	private Transform tf;
	private float normSpeed = 0.0f;
	public float fast = 0.0f;
	public float normal = 0.0f;
	private float slower = .01f;
	private Transform killbox;
	//slower is meant to make it less complex for Designers to put in 
	void Start () {
		tf = GetComponent<Transform>();
		//Removed some items from previous iteration of movement script.
		//Changed information to streamline the design of the level
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W)) {
			tf.position -= tf.up * speed * normSpeed * slower;
		}
		//Move Forward
		//fixed rate of speed
		if (Input.GetKey (KeyCode.A)) {
			tf.Rotate (Vector3.forward, Time.deltaTime * rotationSpeed);
		}
		//turn left
		if (Input.GetKey (KeyCode.S)) {
			tf.position += tf.up * speed * slower;
		}
		//Move backwards - Slower than forward
		if (Input.GetKey (KeyCode.D)) {
			tf.Rotate (-Vector3.forward, Time.deltaTime * rotationSpeed);
		}
		//turn right
		if (Input.GetKey (KeyCode.LeftShift)) {
			normSpeed = fast;
		} else {
			normSpeed = normal;
			//Set for the OverDrive mechanic
		}
	}
	public void OnTriggerEnter2D ( Collider2D otherCollider) {
		if (killbox = GameObject.FindGameObjectWithTag ("Killbox").transform) {
			Destroy (this.gameObject);
		}
		//Player dies when touching the anything
	}
}
