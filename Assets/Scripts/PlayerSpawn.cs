﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {
	public GameObject playerPrefab;
	//attatch player prefab that will be spawned
	public Transform spawnPoint;
	//location the player will spawn at
	private Transform player;
	public Transform playerHolder;
	public int playerNumber = 0;

	void Start () {
		spawnPoint = GetComponent<Transform> ();
		SpawnPlayer ();
		playerNumber = playerNumber + 1;
		playerHolder = GetComponent<Transform> ();
		//get the location
	}

	void SpawnPlayer () {
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
		//spawn the player
	}
	void Update () {
		playerHolder = GameObject.FindGameObjectWithTag ("Player").transform;
		if (playerHolder == null){
			SpawnPlayer ();
		}
	}
}
